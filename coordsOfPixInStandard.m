% inputs:
%--------
% i,j : indexes of a pixel
% lens: the array [ c; xc; yc; zc]
% sens: the array [ f, w, h, rw, rh];
% output: 
%--------
% the coordinates of the center of the (i,j)-th pixel 
% in the standard coordinate system
function coords = coordsOfPixInStandard(i,j,lens,sens)
f=sens(1);
w=sens(2);
h=sens(3);
rw=sens(4);
rh=sens(5);
cc=[((2*i-1)*w/rw-w)/2;((2*j-1)*h/rh-h)/2;f];
coords=lens(:,1)+lens(:,2:end)*cc;
end
