% inputs:
%--------
% i,j   : indexes of a pixel
% k     : the index of a patch
% lens  : the array [ c; xc; yc; zc]
% sens  : the array [ f, w, h, rw, rh]; 
% Points: the v*3 array containing the matrices of the scene
% Scene : the m*3 array which k-th row contains the 
%         indexes in Points of the vertices of the k-th patch
% output: 
%--------
function depth  = intersectRayPatch(i,j,k,lens,sens, Points, Scene)
c=lens(:,1);
f=sens(1);
w=sens(2);
h=sens(3);
rw=sens(4);
rh=sens(5);
p=zeros(3);
p(:,1)=Points(Scene(k,1),:);
p(:,2)=Points(Scene(k,2),:);
p(:,3)=Points(Scene(k,3),:);
% pixel in c
pc=[((2*i-1)*w/rw-w)/2; ((2*j-1)*h/rh-h)/2; f];
% pixel-c in s
ps=lens(:,2:end)*pc;
A=zeros(3);
A(:,1)=-ps;
A(:,2)=p(:,2)-p(:,1);
A(:,3)=p(:,3)-p(:,1);
b=lens(:,1)-p(:,1);
% solve the affine coordinate (u1 u2 u3), u1 is the coordinate of the ray,
% (u2 u3) is the coordinate of the triangle
u=A\b;
if u(1)>=0 && u(2)>=0 && u(3)>=0 && u(2)+u(3)<=1
    depth=u(1)*norm(ps);
else
    depth=+Inf;
end
end
