% inputs:
%--------
% i,j : indexes of a pixel
% lens: the array [ c; xc; yc; zc]
% sens: the array [ f, w, h, rw, rh];  
% output: 
%--------
% the implicit representation of the line (c,pix(i,j)):
% A : an 2*3 matrix
% w : a 2*1 vector
% s.t the points of the (c,pix(i,j)) are the solutions 
% of Av=w
function [A,w] = systOfEqnsOfRay(i,j, lens,sens)
A=zeros(2,3);
w=zeros(2,1);
cc=[((2*i-1)*sens(2)/sens(4)-sens(2))/2;...
    ((2*j-1)*sens(3)/sens(5)-sens(3))/2;sens(1)];
p=lens(:,2:end)*cc;
A(1,:)=lens(:,2)-proj(lens(:,2),p);
A(2,:)=lens(:,3)-proj(lens(:,3),A(1,:))-proj(lens(:,3),p);
w=A*lens(:,1);
end

function p=proj(a,b)
p=dot(a,b)/norm(b)*b;
end
